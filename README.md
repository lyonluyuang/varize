# About
This is a completely new programming language called ``Varize``. You can find the description of the version in its folder.
# Version
Up till now, varize is still developing, there is no releases, sorry. :-(
# Download
You can download in the release part of this repository for the release verison of ``Varize``.

Also, you may get the latest devlop verison of ``Varize`` by downloading the zip file of this repository.
# Copyright
We give up all the copyrights, including the wiki, the codes and the documents, etc. If you contribute by any means, you are also giving up your copyright. Thank you!
